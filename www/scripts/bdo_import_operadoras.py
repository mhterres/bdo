#!/usr/bin/python
# -*- coding: utf-8 -*-

# Importa operadoras do BDO 
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-01-26
#

import os
import sys
import time
import hashlib
import datetime

from bdo.dbpgsql import DBPgsql
from bdo.config import Config

verbose=False

if len(sys.argv) > 1:

	if sys.argv[1]=="-v":

		verbose=True

cfg=Config()
DB=DBPgsql(cfg)

file_operadoras = "%s/Extra/operadoras_e_spid.csv"	% cfg.bdo_path

if not os.path.exists(file_operadoras):

	print "Arquivo %s não encontrado." % file_operadoras
	sys.exit(1)

if verbose:

	print "Processando arquivo %s" % file_operadoras

i = 0
x = 0

with open(file_operadoras) as f:

	for line in f:

		if i > 0:

			data={}

			campos = (line.replace("\n","").replace("\r","")).split(";")

			data['operadora']=campos[0]
			data['spid']=campos[1]
			data['rn1']=campos[2]

			if not DB.search_operadora(data['operadora']):
			
				DB.insert_operadora(data)

				x = x + 1

			if verbose:

				sys.stdout.write('%i\r' % i)

		i = i +1

hash_file=hashlib.md5(open(file_operadoras, 'rb').read()).hexdigest()
DB.update_update(hash_file,"operadoras")

print "Foram importados %i registros." % x

