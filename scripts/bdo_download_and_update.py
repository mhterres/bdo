#!/usr/bin/python
# -*- coding: utf-8 -*-

# Baixa novos arquivos do BDO
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-01-26
#

import os
import sys
import gzip
import time
import ftplib
import hashlib
import datetime

from subprocess import Popen, PIPE, STDOUT

from bdo.dbpgsql import DBPgsql
from bdo.config import Config
from bdo.mail import Mail

output=""

cfg=Config()
DB=DBPgsql(cfg)
mail=Mail()

pathname = os.path.dirname(sys.argv[0])
path=os.path.abspath(pathname)

file_operadoras = "%s/Extra/operadoras_e_spid.csv"	% cfg.bdo_path
file_prefixos = "%s/Extra/prefixos.csv"	% cfg.bdo_path

ftp = ftplib.FTP(cfg.ftp_host, cfg.ftp_user, cfg.ftp_pwd)

lastfile=DB.search_update('lastfile').split(".")[0]

data_str="20"+lastfile[-2:]+"-"+lastfile[2:4]+"-"+lastfile[:2]+" 00:00:00"
data_tstamp = int(datetime.datetime.strptime(data_str, '%Y-%m-%d %H:%M:%S').strftime("%s"))

now = time.time()

while data_tstamp < now:

	dlFile=data_str[8:10]+data_str[5:7]+data_str[2:4]+".Log"
	localFile="%s/%s" % (cfg.bdo_path,dlFile)

	print "Baixando arquivo %s." % dlFile

	lFile=open(localFile, "wb")

	try:

		ftp.retrbinary('RETR %s' % dlFile, lFile.write)

	except:

		print "Arquivo %s não encontrado." % dlFile
		lFile.close()

	else:

		i=0
		x=0
		y=0

		lastfile=dlFile

		lFile.close()

		print "Processando arquivo %s." % localFile

		with open(localFile) as f:

			for line in f:

				sys.stdout.write("%i\r" % i)
				sys.stdout.flush()

				if i > 0:

					data={}

					campos = (line.replace("\n","").replace("\r","")).split(";")

					data['versionid']=campos[0]
					data['tn']=campos[1]
					data['rn1']=campos[2]


					if len(campos[3])==10:

						campos[3]+=" 00:00:00"

					tstamp = int(datetime.datetime.strptime(campos[3], '%d/%m/%Y %H:%M:%S').strftime("%s"))
#					tstamp = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(tstamp))
					data['data']=time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(tstamp))					
					data['operação']=campos[4]

					search=DB.search_numero(data['tn'])

					if len(search)>1:
	
						if tstamp > int(datetime.datetime.strptime(search[1].strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S').strftime("%s")):
							
							DB.update_numero(data)

							y=y+1

					else:

						DB.insert_numero(data)
		
						x=x+1

				i=i+1

		f.close()

		output+="Arquivo %s processado.\n" % dlFile
		output+="%i registro(s) alterado(s).\n" % y
		output+="%i registro(s) adicionado(s).\n" % x

	data_tstamp += 86400
	data_str=datetime.datetime.fromtimestamp(data_tstamp).strftime('%Y-%m-%d %H:%M:%S')

	
# Diretório Extra - operadoras e prefixos

ftp.cwd("Extra")

# Operadoras
tmpFile="/tmp/operadoras.tmp"

try:
	os.remove(tmpFile)
except: 
	print "Can't delete %s." % tmpFile

print "Baixando arquivo de operadoras."

dlFile = open(tmpFile, "wb")
ftp.retrbinary('RETR operadoras_e_spid.csv', dlFile.write)
dlFile.close()

hash_file=hashlib.md5(open(tmpFile, 'rb').read()).hexdigest()
old_hash=DB.search_update('hash_operadoras')

if hash_file != old_hash:

	print "Processando arquivo de operadoras."

	os.remove(file_operadoras)
	os.rename(tmpFile,file_operadoras)

	cmd = '%s/bdo_import_operadoras.py' % path
	p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	out = p.stdout.read().replace("\r","").replace("\n","")

	output+="Tabela de operadoras atualizada: \n"
	output+="%s\n" % out

# Prefixos

tmpFile="/tmp/prefixos.csv"
tmpFileGz="%s.gz" % tmpFile

try: 
	os.remove(tmpFile)
except:
	print "Can't delete %s." % tmpFile

try: 
	os.remove(tmpFileGz)
except:
	print "Can't delete %s." % tmpFileGz

print "Baixando arquivo de prefixos."

dlFile = open(tmpFileGz, "wb")

ftp.retrbinary('RETR prefixos.csv.gz', dlFile.write)
dlFile.close()

f=gzip.open(tmpFileGz, "rb")
conteudo=f.read()
f.close()

f=file(tmpFile, "wb")
f.write(conteudo)
f.close()


hash_file=hashlib.md5(open(tmpFile, 'rb').read()).hexdigest()
old_hash=DB.search_update('hash_prefixos')

if hash_file != old_hash:

	print "Processando arquivo de prefixos."

	os.remove(file_prefixos)
	os.rename(tmpFile,file_prefixos)

	cmd = '%s/bdo_import_prefixos.py' % path
	p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	out = p.stdout.read().replace("\r","").replace("\n","")

	output+="Tabela de prefixos atualizada: \n"
	output+="%s\n" % out

if output != "":

	if cfg.send_email_update=='1':

		mail.send_msg(cfg,cfg.email_dest,'Tabelas BDO atualizadas',output)
	else:

		print output

DB.update_update(datetime.datetime.now(),'data')
DB.update_update(lastfile,'file')

ftp.close()
