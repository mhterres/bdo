#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		config.py
		Classe da configuração do sistema
		2015/01/26
		Marcelo Hartmann Terres <mhterres@gmail.com>
"""

import os
import sys
import ConfigParser

class Config:

	def __init__(self):

		self.description = 'Configurations'

		configuration = ConfigParser.RawConfigParser()

		pathname = os.path.dirname(sys.argv[0])
		path=os.path.abspath(pathname)
		configuration = ConfigParser.RawConfigParser()
		configuration.read('%s/../config/bdo.conf' % path)

		# files
		self.bdo_path=configuration.get('files','bdo_path')

		# download
		self.ftp_host=configuration.get('download','ftp_host')
		self.ftp_user=configuration.get('download','ftp_user')
		self.ftp_pwd=configuration.get('download','ftp_pwd')

		# database
		self.db_host=configuration.get('database','db_host')
		self.db_port=configuration.get('database','db_port')
		self.db_name=configuration.get('database','db_name')
		self.db_user=configuration.get('database','db_user')
		self.db_pwd=configuration.get('database','db_pwd')

		# email
		self.send_email_update=configuration.get('email','send_email_update')
		self.send_email_zera_contadores=configuration.get('email','send_email_zera_contadores')

		self.email_dest=configuration.get('email','email_dest')
		self.email_send=configuration.get('email','email_send')
		self.email_host=configuration.get('email','email_host')

