#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		dbpgsql.py
		Classe para acesso ao DB Postgres
		2015/01/27
		Marcelo Hartmann Terres <mhterres@gmail.com>
"""
import psycopg2
import psycopg2.extras

class DBPgsql:

	def __init__(self,config):

		self.description = 'Postgresql Database'

		self.dsn = 'dbname=%s host=%s user=%s password=%s' % (config.db_name,config.db_host,config.db_user,config.db_pwd)

		self.conn = psycopg2.connect(self.dsn)

	def insert_operadora(self,data):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:
			curs.execute("INSERT INTO operadoras (operadora, spid, rn1) VALUES(%s, %s , %s);", (data['operadora'],data['spid'],data['rn1']))

		except:
			self.conn.rollback()

		else:
			self.conn.commit()

		curs.close()

	def insert_prefixo(self,data):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:
			curs.execute("INSERT INTO prefixos (operadora, tipo, rn1, ddd, prefixo, mcdu_inicial, mcdu_final, eot, uf, cnl) VALUES(%s, %s , %s, %s, %s, %s, %s, %s, %s, %s);", (data['operadora'],data['tipo'],data['rn1'],data['ddd'],data['prefixo'],data['mcdu_inicial'],data['mcdu_final'],data['eot'],data['uf'],data['cnl']))

		except:
			print "Erro importando: %s, %s, %s, %s" % (data['operadora'],data['rn1'],data['ddd'],data['prefixo'])

			self.conn.rollback()

		else:
			self.conn.commit()
			
		curs.close()

	def insert_portabilidade(self,data):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:
			curs.execute("INSERT INTO portabilidade (versionid, tn, rn1, data, operação, cnl, eot) VALUES(%s, %s , %s, %s, %s, %s, %s);", (data['versionid'],data['tn'],data['rn1'],data['data'],data['operação'],data['cnl'],data['eot']))

		except:
			self.conn.rollback()

		else:
			self.conn.commit()

		curs.close()

	def search_operadora(self,dado):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:
			curs.execute("SELECT * FROM operadoras where operadora=%s;", (dado,))

		except:
			self.conn.rollback()

		else:

			if curs.rowcount:

				self.conn.commit()
				curs.close()
				return True
			else:

				self.conn.commit()
				curs.close()
				return False

	def search_prefixo(self,data,tipo):

		value=False

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		if tipo=="update":

			try:
				curs.execute("SELECT * FROM prefixos where ddd=%s and prefixo=%s and mcdu_inicial=%s and mcdu_final=%s;", (data['ddd'],data['prefixo'],data['mcdu_inicial'],data['mcdu_final']))

			except:
				self.conn.rollback()

			else:

				if curs.rowcount:

					self.conn.commit()
					value=True
#			else:
#
#				curs.execute("SELECT * FROM prefixos where ddd=%s and prefixo=%s;", (data['ddd'],data['prefixo']))
#
#				if curs.rowcount:
#
#					rec=curs.fetchone()
#
#					if data['numero'] >= rec['mcdu_inicial'] and data['numero'] <= rec['mcdu_final']:
#
#						value=True

				else:
					self.conn.commit()

				curs.close()
		return value

	def search_numero(self,dado):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:
			curs.execute("SELECT * FROM portabilidade where TN=%s;", (dado,))

		except:
			self.conn.rollback()

		else:
			if curs.rowcount:

				rec=curs.fetchone()

				self.conn.commit()
				curs.close()
				return [rec['rn1'],rec['data']]
			else:

				self.conn.commit()
				curs.close()
				return [""]

	def update_numero(self,dado):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:
			curs.execute("UPDATE portabilidade SET versionid=%s, rn1=%s, data=%s, operação=%s WHERE tn=%s;", (dado['versionid'],dado['rn1'],dado['data'],dado['operação'],dado['tn']))

		except:
			self.conn.rollback()

		else:
			self.conn.commit()

		curs.close()

	def insert_numero(self,dado):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:
			curs.execute("INSERT INTO portabilidade (versionid, tn, rn1, data, operação) VALUES (%s, %s, %s, %s, %s);", (dado['versionid'],dado['tn'],dado['rn1'],dado['data'],dado['operação']))

		except:
			self.conn.rollback()

		else:
			self.conn.commit()

		curs.close()
	
	def search_update(self,field):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:
			curs.execute("SELECT * from update;")

		except:
			self.conn.rollback()
			value=""

		else:
			rec=curs.fetchone()

			value=rec[field]

			self.conn.commit()

		curs.close()

		return value

	def update_update(self,dado,tipo):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		if tipo=='data':

			try:
				curs.execute("UPDATE update SET lastdate=%s;", (dado,))

			except:
				self.conn.rollback()

			else:

				self.conn.commit()

		elif tipo=='file':

			try:
				curs.execute("UPDATE update SET lastfile=%s;", (dado,))

			except:
				self.conn.rollback()

			else:
				self.conn.commit()

		elif tipo=='operadoras':

			try:
				curs.execute("UPDATE update SET hash_operadoras=%s;", (dado,))

			except:
				self.conn.rollback()

			else:

				self.conn.commit()

		else:

			try:
				curs.execute("UPDATE update SET hash_prefixos=%s;", (dado,))

			except:
				self.conn.rollback()

			else:

				self.conn.commit()

		curs.close()

		return True

	#
	# defs específico para script bdo_zera_contadores.py
	#

	def lista_acessos(self):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:
			curs.execute("SELECT * from acessos order by id;")

		except:
			self.conn.rollback()

		else:

			self.conn.commit()

		return curs

	def update_acessos(self,codigo):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		
		try:
			curs.execute("UPDATE acessos SET consultas_cont=0 where código=%s;", (codigo,)) 

		except:
			self.conn.rollback()

		else:
			self.conn.commit()

		curs.close()


	#
	# Usado para as operações web
	#

	def operadora_numero(self,numero):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:
			curs.execute("SELECT p.tn,p.rn1,o.operadora FROM portabilidade as p,operadoras as o WHERE p.tn=%s and o.rn1=p.rn1;", (numero, ))

		except:
			self.conn.rollback()

		else:

			if curs.rowcount:

				rec=curs.fetchone()

				dado={}
				dado['numero']=rec['tn']
				dado['rn1']=rec['rn1']
				dado['operadora']=rec['operadora']
				dado['tipo']='P'
			else:

				ddd=numero[0:2]

				if len(numero)==10:

					prefixo=numero[2:6]
					alt_prefixo="9%s" % prefixo
				else:

					prefixo=numero[2:7]
					alt_prefixo=prefixo[1:]

				self.conn.commit()

				alt_numero="%s%s%s" % (ddd,alt_prefixo,numero[-4:])

				try:
					curs.execute("SELECT p.tn,p.rn1,o.operadora FROM portabilidade as p,operadoras as o WHERE p.tn=%s and o.rn1=p.rn1;", (alt_numero, ))

				except:
					self.conn.rollback()
					return "ND"

				else:

					if curs.rowcount:

						rec=curs.fetchone()
		
						dado={}
						dado['numero']=rec['tn']
						dado['rn1']=rec['rn1']
						dado['operadora']=rec['operadora']
						dado['tipo']='P'
					else:

						self.conn.commit()

						try:

							curs.execute("SELECT p.rn1,p.ddd,p.prefixo,o.operadora from prefixos as p, operadoras as o where p.ddd=%s and p.prefixo=%s and o.rn1=p.rn1;", (ddd,prefixo))

						except:
							self.conn.rollback()

						else:

							if curs.rowcount:

								rec=curs.fetchone()

								dado={}
								dado['numero']=numero
								dado['rn1']=rec['rn1']
								dado['operadora']=rec['operadora']
								dado['tipo']='O'

								self.conn.commit()
							else:

								if len(numero)==11 and prefixo[0:1]=="9":

									# Tenta remover o primeiro dígito caso seja celular com 9 dígitos

									prefixo = prefixo[1:]

									self.conn.commit()

									curs2 = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

									try:
										curs2.execute("SELECT p.rn1,p.ddd,p.prefixo,o.operadora from prefixos as p, operadoras as o where p.ddd=%s and p.prefixo=%s and o.rn1=p.rn1;", (ddd,prefixo))
								
									except:
										self.conn.rollback()

									else:

										if curs2.rowcount:

											rec=curs2.fetchone()

									 		dado={}
											dado['numero']=numero
											dado['rn1']=rec['rn1']
											dado['operadora']=rec['operadora']
											dado['tipo']='O'
										else:

											dado={}
											dado['tipo']='ND'
				
										self.conn.commit()

								elif len(numero)==10:

									# Tenta ainda inserir o 9 no prefixo
	
									prefixo="9" + prefixo
	
									self.conn.commit()

									curs2 = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

									try:
										curs2.execute("SELECT p.rn1,p.ddd,p.prefixo,o.operadora from prefixos as p, operadoras as o where p.ddd=%s and p.prefixo=%s and o.rn1=p.rn1;", (ddd,prefixo))
								
									except:
										self.conn.rollback()
			
									else:

										if curs2.rowcount:

											rec=curs2.fetchone()

											dado={}
											dado['numero']=numero
											dado['rn1']=rec['rn1']
											dado['operadora']=rec['operadora']
											dado['tipo']='O'
										else:

											dado={}
											dado['tipo']='ND'

										self.conn.commit()
								else:
						
									dado={}
									dado['tipo']='ND'

									self.conn.commit()

			curs.close()

		return dado

		
	def search_codigo_acesso(self,codigo):

		if len(codigo)==0:

			return {}

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor) 
 
 		try:
			curs.execute("SELECT * FROM acessos where código=%s;", (codigo,)) 

		except:

			self.conn.rollback()

			rec={}

		else:
 
			if curs.rowcount: 

			 	rec=curs.fetchone() 
			else:

				rec={}

			self.conn.commit()

		curs.close() 

		return rec

	def info_acesso(self,codigo):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:
			curs.execute("SELECT * FROM acessos where código=%s;", (codigo,)) 

		except:
			self.conn.rollback()
		
		else:
			self.conn.commit()

		return curs

	def update_acesso(self,codigo,contador):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:
			curs.execute("UPDATE acessos SET consultas_cont=%s WHERE código=%s;", (contador,codigo))

		except:
			self.conn.rollback()

		else:
			self.conn.commit()

		curs.close()


