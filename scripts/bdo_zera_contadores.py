#!/usr/bin/python
# -*- coding: utf-8 -*-

# Zera os contadores de acesso segundo configurações
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-01-27
#

from bdo.dbpgsql import DBPgsql
from bdo.config import Config
from bdo.mail import Mail


from datetime import datetime

now = datetime.now()

cfg=Config()
DB=DBPgsql(cfg)
mail=Mail()

cursor=DB.lista_acessos()

output=""
for row in cursor:

	if row['tipo']=="M":

		if row['dia_renovacao']==now.day:

			DB.update_acessos(row['código'])

			output+="%s - %s - Contador zerado.\n" % (row['código'],row['nome'])
		else:

			output+="%s - %s - %i/%i consultas. Nada a fazer, renovação somente dia %i.\n" % (row['código'],row['nome'],row['consultas_cont'],row['consultas_max'],row['dia_renovacao'])

	elif row['tipo']=="A":

		output+="%s - %s - %i/%i consultas. Nada a fazer, cliente com plano avulso.\n" % (row['código'],row['nome'],row['consultas_cont'],row['consultas_max'])
	else:

		output+="%s - %s - Nada a fazer, cliente com plano ilimitado.\n" % (row['código'],row['nome'])

if output != "":
    if cfg.send_email_zera_contadores=='1':

        mail.send_msg(cfg,cfg.email_dest,'Contadores de acesso ao BDO',output)
    else:

        print output


cursor.close()


