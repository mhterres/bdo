--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

DROP INDEX public.prefixos_full;
DROP INDEX public.prefixos_ddd_prefixo;
DROP INDEX public.portabilidade_tn;
DROP INDEX public.portabilidade_data;
ALTER TABLE ONLY public.prefixos DROP CONSTRAINT prefixos_pkey;
ALTER TABLE ONLY public.portabilidade DROP CONSTRAINT portabilidade_pkey;
ALTER TABLE ONLY public.operadoras DROP CONSTRAINT operadoras_spid_key;
ALTER TABLE ONLY public.operadoras DROP CONSTRAINT operadoras_rn1_key;
ALTER TABLE ONLY public.operadoras DROP CONSTRAINT operadoras_pkey;
ALTER TABLE ONLY public.operadoras DROP CONSTRAINT operadoras_operadora_key;
ALTER TABLE ONLY public.acessos DROP CONSTRAINT acessos_pkey;
ALTER TABLE ONLY public.acessos DROP CONSTRAINT "acessos_código_key";
DROP TABLE public.update;
DROP TABLE public.prefixos;
DROP SEQUENCE public.prefixos_id_seq;
DROP TABLE public.portabilidade;
DROP SEQUENCE public.portabilidade_id_seq;
DROP TABLE public.operadoras;
DROP SEQUENCE public.operadoras_id_seq;
DROP TABLE public.acessos;
DROP SEQUENCE public.acessos_id_seq;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: acessos_id_seq; Type: SEQUENCE; Schema: public; Owner: bdo
--

CREATE SEQUENCE acessos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 1000000
    CACHE 1;


ALTER TABLE public.acessos_id_seq OWNER TO bdo;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acessos; Type: TABLE; Schema: public; Owner: bdo; Tablespace: 
--

CREATE TABLE acessos (
    id bigint DEFAULT nextval('acessos_id_seq'::regclass) NOT NULL,
    "código" character(8) NOT NULL,
    nome character varying(150) NOT NULL,
    consultas_max bigint DEFAULT 0 NOT NULL,
    consultas_cont bigint DEFAULT 0 NOT NULL,
    tipo character(1) NOT NULL,
    dia_renovacao integer
);


ALTER TABLE public.acessos OWNER TO bdo;

--
-- Name: operadoras_id_seq; Type: SEQUENCE; Schema: public; Owner: bdo
--

CREATE SEQUENCE operadoras_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000
    CACHE 1;


ALTER TABLE public.operadoras_id_seq OWNER TO bdo;

--
-- Name: operadoras; Type: TABLE; Schema: public; Owner: bdo; Tablespace: 
--

CREATE TABLE operadoras (
    id bigint DEFAULT nextval('operadoras_id_seq'::regclass) NOT NULL,
    operadora character varying(30) NOT NULL,
    spid bigint NOT NULL,
    rn1 bigint NOT NULL
);


ALTER TABLE public.operadoras OWNER TO bdo;

--
-- Name: portabilidade_id_seq; Type: SEQUENCE; Schema: public; Owner: bdo
--

CREATE SEQUENCE portabilidade_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000000
    CACHE 1;


ALTER TABLE public.portabilidade_id_seq OWNER TO bdo;

--
-- Name: portabilidade; Type: TABLE; Schema: public; Owner: bdo; Tablespace: 
--

CREATE TABLE portabilidade (
    id bigint DEFAULT nextval('portabilidade_id_seq'::regclass) NOT NULL,
    versionid bigint NOT NULL,
    tn bigint NOT NULL,
    rn1 bigint NOT NULL,
    data timestamp without time zone NOT NULL,
    "operação" integer NOT NULL,
    cnl bigint,
    eot integer
);


ALTER TABLE public.portabilidade OWNER TO bdo;

--
-- Name: prefixos_id_seq; Type: SEQUENCE; Schema: public; Owner: bdo
--

CREATE SEQUENCE prefixos_id_seq
    START WITH 1
    INCREMENT BY 11
    NO MINVALUE
    MAXVALUE 10000000
    CACHE 1;


ALTER TABLE public.prefixos_id_seq OWNER TO bdo;

--
-- Name: prefixos; Type: TABLE; Schema: public; Owner: bdo; Tablespace: 
--

CREATE TABLE prefixos (
    id bigint DEFAULT nextval('prefixos_id_seq'::regclass) NOT NULL,
    operadora character varying(100) NOT NULL,
    tipo character varying(10) NOT NULL,
    rn1 bigint NOT NULL,
    ddd integer NOT NULL,
    prefixo integer NOT NULL,
    mcdu_inicial integer NOT NULL,
    mcdu_final integer NOT NULL,
    eot integer NOT NULL,
    uf character(2) NOT NULL,
    cnl bigint NOT NULL
);


ALTER TABLE public.prefixos OWNER TO bdo;

--
-- Name: update; Type: TABLE; Schema: public; Owner: bdo; Tablespace: 
--

CREATE TABLE update (
    lastdate timestamp without time zone NOT NULL,
    lastfile character varying(100) NOT NULL,
    hash_operadoras character(32) NOT NULL,
    hash_prefixos character(32) NOT NULL
);


ALTER TABLE public.update OWNER TO bdo;

--
-- Name: acessos_código_key; Type: CONSTRAINT; Schema: public; Owner: bdo; Tablespace: 
--

ALTER TABLE ONLY acessos
    ADD CONSTRAINT "acessos_código_key" UNIQUE ("código");


--
-- Name: acessos_pkey; Type: CONSTRAINT; Schema: public; Owner: bdo; Tablespace: 
--

ALTER TABLE ONLY acessos
    ADD CONSTRAINT acessos_pkey PRIMARY KEY (id);


--
-- Name: operadoras_operadora_key; Type: CONSTRAINT; Schema: public; Owner: bdo; Tablespace: 
--

ALTER TABLE ONLY operadoras
    ADD CONSTRAINT operadoras_operadora_key UNIQUE (operadora);


--
-- Name: operadoras_pkey; Type: CONSTRAINT; Schema: public; Owner: bdo; Tablespace: 
--

ALTER TABLE ONLY operadoras
    ADD CONSTRAINT operadoras_pkey PRIMARY KEY (id);


--
-- Name: operadoras_rn1_key; Type: CONSTRAINT; Schema: public; Owner: bdo; Tablespace: 
--

ALTER TABLE ONLY operadoras
    ADD CONSTRAINT operadoras_rn1_key UNIQUE (rn1);


--
-- Name: operadoras_spid_key; Type: CONSTRAINT; Schema: public; Owner: bdo; Tablespace: 
--

ALTER TABLE ONLY operadoras
    ADD CONSTRAINT operadoras_spid_key UNIQUE (spid);


--
-- Name: portabilidade_pkey; Type: CONSTRAINT; Schema: public; Owner: bdo; Tablespace: 
--

ALTER TABLE ONLY portabilidade
    ADD CONSTRAINT portabilidade_pkey PRIMARY KEY (id);


--
-- Name: prefixos_pkey; Type: CONSTRAINT; Schema: public; Owner: bdo; Tablespace: 
--

ALTER TABLE ONLY prefixos
    ADD CONSTRAINT prefixos_pkey PRIMARY KEY (id);


--
-- Name: portabilidade_data; Type: INDEX; Schema: public; Owner: bdo; Tablespace: 
--

CREATE INDEX portabilidade_data ON portabilidade USING btree (data);


--
-- Name: portabilidade_tn; Type: INDEX; Schema: public; Owner: bdo; Tablespace: 
--

CREATE INDEX portabilidade_tn ON portabilidade USING btree (tn);


--
-- Name: prefixos_ddd_prefixo; Type: INDEX; Schema: public; Owner: bdo; Tablespace: 
--

CREATE INDEX prefixos_ddd_prefixo ON prefixos USING btree (ddd);


--
-- Name: prefixos_full; Type: INDEX; Schema: public; Owner: bdo; Tablespace: 
--

CREATE INDEX prefixos_full ON prefixos USING btree (ddd, prefixo, mcdu_inicial, mcdu_final);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--


