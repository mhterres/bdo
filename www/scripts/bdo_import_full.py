#!/usr/bin/python
# -*- coding: utf-8 -*-

# Importa a base completa do BDO 
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-01-26
#

import os
import sys
import time
import datetime

from bdo.dbpgsql import DBPgsql
from bdo.config import Config

cfg=Config()
DB=DBPgsql(cfg)

file_full = "%s/FULL/BASE_COMPLETA.TXT" % cfg.bdo_path

if not os.path.exists(file_full):

	print "Arquivo %s não encontrado." % file_full
	sys.exit(1)

print "Processando arquivo %s" % file_full

i = 0 

with open(file_full) as f:
	for line in f:

		if i > 0:

			data={}

			campos = (line.replace("\n","").replace("\r","")).split(";")

			data['versionid']=campos[0]
			data['tn']=campos[1]
			data['rn1']=campos[2]

			tstamp = int(datetime.datetime.strptime(campos[3], '%Y-%m-%d %H:%M:%S').strftime("%s"))
			tstamp = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(tstamp))
			data['data']=tstamp

			data['operação']=campos[4]

			if campos[5] == '':

				data['cnl']=0
			else:

				data['cnl']=campos[5]

			data['eot']=campos[6]

			DB.insert_portabilidade(data)

			sys.stdout.write('%i\r' % i)

		i = i +1

print "Foram importados %i registros." % i
