#!/bin/bash

DIR=/var/backups/pgsql
KEEP=7

if [ ! -d $DIR ]; then
  echo "Não foi possível acessar o diretório $DIR" && exit 1
  fi

cd $DIR || exit 1

DATA=`date +"%Y-%m-%d"`
umask 077

su - postgres -c "/usr/bin/pg_dumpall -c" | /bin/gzip -c > /tmp/$DATA.sql.gz
mv /tmp/$DATA.sql.gz $DIR/$DATA.sql.gz

/usr/bin/find "$DIR" -mtime +$KEEP -exec rm -f {} \;

