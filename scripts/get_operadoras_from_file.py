#!/usr/bin/python
# -*- coding: utf-8 -*-

# Informa as operadoras dos números constantes no arquivo texto
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-01-27
#

import sys

from bdo.dbpgsql import DBPgsql
from bdo.config import Config

cfg=Config()
DB=DBPgsql(cfg)

import cgi, os
import cgitb; cgitb.enable()

form = cgi.FieldStorage()

fileitem = form['filename']
formato = form.getvalue('formato')

if fileitem.filename:

	fn = os.path.basename(fileitem.filename)
	open('/tmp/' + fn, 'wb').write(fileitem.file.read())

	if formato=="html":

		output = "<table border=0>"
		output+="  <tr>"
		output+="	 <td><b>N&uacute;mero</b></td>"
		output+="	 <td><b>RN1</b></td>"
		output+="	 <td><b>Operadora</b></td>"
		output+="	 <td><b>Tipo</b></td>"
	else:

		output ="Numero,RN1,Operadora,Tipo\n"
	
	with open('/tmp/%s' % fn) as f:

		for line in f:

			if formato=="html":

				output+="  <tr>"

			line=(line.replace("\r","").replace("\n","")).strip()
			dado=DB.operadora_numero(line)

			if dado['tipo'] != "ND":

				if dado['tipo']=="P":

					tipo="PORTABILIDADE"
				else:

					tipo="ORIGEM"
		
				if formato=="html":

					output += "	 <td>%s</td>" % line
					output += "	 <td>%s</td>" % dado['rn1']
					output += "	 <td>%s</td>" % dado['operadora']
					output += "	 <td>%s</td>" % tipo
				else:

					output += "%s,%s,%s,%s\n" % (line,dado['rn1'],dado['operadora'],tipo)

			else:

				if formato=="html":

					output += "	 <td>%s</td>" % line
					output += "	 <td>ND</td>" 
					output += "	 <td>ND</td>" 
					output += "	 <td>ND</td>" 
				else:

					output += "%s,ND,ND,ND\n" % line
			
			if formato=="html":

				output+="  </tr>"

	if formato=="html":

		output+="</table>"
else:
	output = 'Erro no envio do arquivo %s.' % fn

if formato=="html":
	print "Content-type:text/html\r\n\r\n"
	print output
else:
	print "Content-type:text/plain\r\n"
	print output

