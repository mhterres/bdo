#!/usr/bin/python
# -*- coding: utf-8 -*-

# Importa prefixos do BDO 
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-01-26
#

import os
import sys
import time
import codecs
import hashlib
import datetime

from bdo.dbpgsql import DBPgsql
from bdo.config import Config

verbose=False

if len(sys.argv) > 1:

	if sys.argv[1]=="-v":

		verbose=True

cfg=Config()
DB=DBPgsql(cfg)

file_prefixos = "%s/Extra/prefixos.csv"	% cfg.bdo_path

if not os.path.exists(file_prefixos):

	print "Arquivo %s não encontrado." % file_prefixos
	sys.exit(1)

if verbose:

	print "Processando arquivo %s" % file_prefixos

i = 0
x = 0

with codecs.open(file_prefixos,mode='r',encoding='IBM855') as f:

	for line in f:

		if i > 0:

			data={}

			campos = (line.replace("\n","").replace("\r","")).split(";")

			data['operadora']=campos[0].replace("'","")
			data['tipo']=campos[1]
			data['rn1']=campos[2]
			data['ddd']=campos[3]
			data['prefixo']=campos[4]
			data['mcdu_inicial']=campos[5]
			data['mcdu_final']=campos[6]
			data['eot']=campos[7]
			data['uf']=campos[8]
			data['cnl']=campos[9]

			if not DB.search_prefixo(data,'update'):
			
				DB.insert_prefixo(data)

				x = x + 1

			if verbose:

				sys.stdout.write('%i\r' % i)

		i = i +1


hash_file=hashlib.md5(open(file_prefixos, 'rb').read()).hexdigest()
DB.update_update(hash_file,"prefixos")

print "Foram importados %i registros." % x

