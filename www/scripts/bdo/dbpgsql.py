#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		dbpgsql.py
		Classe para acesso ao DB Postgres
		2015/01/27
		Marcelo Hartmann Terres <mhterres@gmail.com>
"""
import psycopg2
import psycopg2.extras

class DBPgsql:

	def __init__(self,config):

		self.description = 'Postgresql Database'

		self.dsn = 'dbname=%s host=%s user=%s password=%s' % (config.db_name,config.db_host,config.db_user,config.db_pwd)

		self.conn = psycopg2.connect(self.dsn)

	def insert_operadora(self,data):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute("INSERT INTO operadoras (operadora, spid, rn1) VALUES(%s, %s , %s);", (data['operadora'],data['spid'],data['rn1']))
		self.conn.commit()
		curs.close()

	def insert_prefixo(self,data):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		try:

			curs.execute("INSERT INTO prefixos (operadora, tipo, rn1, ddd, prefixo, mcdu_inicial, mcdu_final, eot, uf, cnl) VALUES(%s, %s , %s, %s, %s, %s, %s, %s, %s, %s);", (data['operadora'],data['tipo'],data['rn1'],data['ddd'],data['prefixo'],data['mcdu_inicial'],data['mcdu_final'],data['eot'],data['uf'],data['cnl']))
		except:

			print "Erro importando: %s, %s, %s, %s" % (data['operadora'],data['rn1'],data['ddd'],data['prefixo'])

		self.conn.commit()
		curs.close()

	def insert_portabilidade(self,data):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute("INSERT INTO portabilidade (versionid, tn, rn1, data, operação, cnl, eot) VALUES(%s, %s , %s, %s, %s, %s, %s);", (data['versionid'],data['tn'],data['rn1'],data['data'],data['operação'],data['cnl'],data['eot']))
		self.conn.commit()
		curs.close()

	def search_operadora(self,dado):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		curs.execute("SELECT * FROM operadoras where operadora=%s;", (dado,))

		if curs.rowcount:

			curs.close()
			return True
		else:
			curs.close()
			return False

	def search_prefixo(self,data,tipo):

		value=False

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		if tipo=="update":

			curs.execute("SELECT * FROM prefixos where ddd=%s and prefixo=%s and mcdu_inicial=%s and mcdu_final=%s;", (data['ddd'],data['prefixo'],data['mcdu_inicial'],data['mcdu_final']))

			if curs.rowcount:

				value=True
			else:

				curs.execute("SELECT * FROM prefixos where ddd=%s and prefixo=%s;", (data['ddd'],data['prefixo']))

				if curs.rowcount:

					rec=curs.fetchone()

					if data['numero'] >= rec['mcdu_inicial'] and data['numero'] <= rec['mcdu_final']:

						value=True

			curs.close()
		return value

	def search_numero(self,dado):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		curs.execute("SELECT * FROM portabilidade where TN=%s;", (dado,))

		if curs.rowcount:

			rec=curs.fetchone()

			curs.close()
			return [rec['rn1'],rec['data']]
		else:

			curs.close()
			return [""]

	def update_numero(self,dado):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute("UPDATE portabilidade SET versionid=%s, rn1=%s, data=%s, operação=%s WHERE tn=%s;", (dado['versionid'],dado['rn1'],dado['data'],dado['operação'],dado['tn']))
		self.conn.commit()
		curs.close()

	def insert_numero(self,dado):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute("INSERT INTO portabilidade (versionid, tn, rn1, data, operação) VALUES (%s, %s, %s, %s, %s);", (dado['versionid'],dado['tn'],dado['rn1'],dado['data'],dado['operação']))
		self.conn.commit()
		curs.close()

	
	def search_update(self,field):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		curs.execute("SELECT * from update;")

		rec=curs.fetchone()

		value=rec[field]

		curs.close()

		return value

	def update_update(self,dado,tipo):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		if tipo=='data':

			curs.execute("UPDATE update SET lastdate=%s;", (dado,))

		elif tipo=='file':

			curs.execute("UPDATE update SET lastfile=%s;", (dado,))

		elif tipo=='operadoras':

			curs.execute("UPDATE update SET hash_operadoras=%s;", (dado,))

		else:

			curs.execute("UPDATE update SET hash_prefixos=%s;", (dado,))

		self.conn.commit()
		curs.close()

		return True

	#
	# defs específico para script bdo_zera_contadores.py
	#

	def lista_acessos(self):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute("SELECT * from acessos order by id;")

		return curs

	def update_acessos(self,codigo):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute("UPDATE acessos SET consultas_cont=0 where código=%s;", (codigo,)) 

		self.conn.commit()
		curs.close()


	#
	# Usado para as operações web
	#

	def operadora_numero(self,numero):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		curs.execute("SELECT p.tn,p.rn1,o.operadora FROM portabilidade as p,operadoras as o WHERE p.tn=%s and o.rn1=p.rn1;", (numero, ))

		if curs.rowcount:

			rec=curs.fetchone()

			dado={}
			dado['numero']=rec['tn']
			dado['rn1']=rec['rn1']
			dado['operadora']=rec['operadora']
			dado['tipo']='P'
		else:

			if len(numero)==10:

				ddd=numero[0:2]
				prefixo=numero[2:6]
			else:

				ddd=numero[0:2]
				prefixo=numero[2:7]

			curs.execute("SELECT p.rn1,p.ddd,p.prefixo,o.operadora from prefixos as p, operadoras as o where p.ddd=%s and p.prefixo=%s and o.rn1=p.rn1;", (ddd,prefixo))

			if curs.rowcount:

				rec=curs.fetchone()

				dado={}
				dado['numero']=numero
				dado['rn1']=rec['rn1']
				dado['operadora']=rec['operadora']
				dado['tipo']='O'
			else:

				# Tenta ainda inserir o 9 no prefixo

				if len(numero)==10:

					prefixo="9" + prefixo

					curs2 = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

					curs2.execute("SELECT p.rn1,p.ddd,p.prefixo,o.operadora from prefixos as p, operadoras as o where p.ddd=%s and p.prefixo=%s and o.rn1=p.rn1;", (ddd,prefixo))

					if curs2.rowcount:

	  					rec=curs2.fetchone()

				 		dado={}
						dado['numero']=numero
		  				dado['rn1']=rec['rn1']
				 		dado['operadora']=rec['operadora']
						dado['tipo']='O'

	  				else:

						dado={}
						dado['tipo']='ND'

					curs2.close()
				else:
						
					dado={}
					dado['tipo']='ND'

		curs.close()

		return dado

		
	def search_codigo_acesso(self,codigo):

		if len(codigo)==0:

			return {}

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor) 
 
		curs.execute("SELECT * FROM acessos where código=%s;", (codigo,)) 
 
		if curs.rowcount: 

		 	rec=curs.fetchone() 
		else:

			rec={}

		curs.close() 
		return rec

	def info_acesso(self,codigo):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		curs.execute("SELECT * FROM acessos where código=%s;", (codigo,)) 

		return curs

	def update_acesso(self,codigo,contador):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		curs.execute("UPDATE acessos SET consultas_cont=%s WHERE código=%s;", (contador,codigo))

		self.conn.commit()
		curs.close()


