#!/usr/bin/python
# -*- coding: utf-8 -*-

# Webservice para retornar a operadora de um determinado número
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-01-27
#

import web
from bdo.dbpgsql import DBPgsql
from bdo.config import Config

cfg=Config()
DB=DBPgsql(cfg)

urls = (
	'/numero/', 'get_operadora',
	'/info/', 'get_info',
	'/acessos/(.*)', 'get_acessos'
)

app = web.application(urls, globals())

class get_operadora:
	def GET(self):

		getCodigo=web.input(codigo="INVALIDO")
		getNumero=web.input(numero="")

		codigo=getCodigo.codigo
		numero=getNumero.numero

		if not VerificaNumero(numero):

			output="ERRO"
		else:

			acesso=DB.info_acesso(codigo)

			if not acesso.rowcount:

				output='Cliente inexistente.'
				acesso.close()
			else:

				rec=acesso.fetchone()

				consulta=True

				if rec['tipo'] != "I":

					if rec['consultas_cont']>=rec['consultas_max']:

						output='Acesso negado.'
						consulta=False

				contador=rec['consultas_cont']+1

				acesso.close()

				if consulta:

					dado=DB.operadora_numero(numero)

					output=""

					if dado['tipo'] !="ND":

						output=dado['rn1']

					else:

						output="ERRO"

					DB.update_acesso(codigo,contador)

		return output

class get_info:

	def GET(self):

		getCodigo=web.input(codigo="INVALIDO")
		getNumero=web.input(numero="")

		codigo=getCodigo.codigo
		numero=getNumero.numero

		if not VerificaNumero(numero):

			output="ERRO"
		else:

			acesso=DB.info_acesso(codigo)

			if not acesso.rowcount:

				output='Cliente inexistente.'
				acesso.close()
			else:

				rec=acesso.fetchone()

				consulta=True

				if rec['tipo'] != "I":

					if rec['consultas_cont']>=rec['consultas_max']:

						output='Acesso negado.'
						consulta=False

				contador=rec['consultas_cont']+1

				acesso.close()

				if consulta:

					dado=DB.operadora_numero(numero)

					output=""

					if dado['tipo']=="ND":

						output+="ERRO"
					else:
	
						if dado['tipo']=="P":

							tipo="PORTABILIDADE"
						else:

							tipo="ORIGEM"

						output+='<?xml version="1.0" encoding="UTF-8"?>\n'
						output+="<numero>\n"
						output+="  <tn>%s</tn>\n" % dado['numero']
						output+="  <rn1>%s</rn1>\n" % dado['rn1']
						output+="  <operadora>%s</operadora>\n" % dado['operadora']
						output+="  <tipo>%s</tipo>\n" % tipo
						output+="</numero>"

					DB.update_acesso(codigo,contador)

		return output

class get_acessos:

	def GET(self,codigo):

		dado=DB.search_codigo_acesso(codigo)

		if len(dado)==0:

			output="ERRO"
		else:

			output=""

			if dado['tipo']=="I":

				tipo="ILIMITADO"

			elif dado['tipo']=="M":

				tipo="MENSAL"
			else:

				tipo="AVULSO"

			output+='<?xml version="1.0" encoding="UTF-8"?>\n'
			output+="<acesso>\n"
			output+="  <codigo>%s</codigo>\n" % dado['código']
			output+="  <nome>%s</nome>\n" % dado['nome']
			output+="  <consultas_atual>%i</consultas_atual>\n" % dado['consultas_cont']
			output+="  <consultas_maximo>%i</consultas_maximo>\n" % dado['consultas_max']
			output+="  <tipo>%s</tipo>\n" % tipo
			output+="  <dia_vencimento>%i</dia_vencimento>\n" % dado['dia_renovacao']
			output+="</acesso>"

		return output

def VerificaNumero(numero):

	if len(numero)<10:

		return False

	try:
		valor = int(numero)
	except ValueError:

		return False

	return True

if __name__  == "__main__":
	app.run()
