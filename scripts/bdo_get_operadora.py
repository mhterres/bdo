#!/usr/bin/python
# -*- coding: utf-8 -*-

# Informa a operadora de um número
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-01-27
#

import sys

from bdo.dbpgsql import DBPgsql
from bdo.config import Config

if len(sys.argv) > 1:

	numero = sys.argv[1]

	try: 
		valor = int(numero) 
	except ValueError: 
		print  "O parâmetro deve ser um número de telefone." 
		sys.exit(0)

	if len(numero)<10:
		print  "O parâmetro deve ser um número de telefone com DDD." 
		sys.exit(0)

else:

	print "Informe o número a ser pesquisado com DDD."
	print "Ex: ./bdo_get_operadora.py 5199998888"
	sys.exit(0)

cfg=Config()
DB=DBPgsql(cfg)

dado=DB.operadora_numero(numero)

if dado['tipo']=="ND":

	print "Não foi possível encontrar o número %s na base de dados." % numero
else:

	if dado['tipo']=="P":

		tipo="Número portado para operadora %s" % dado['operadora']
	else:

		tipo="Número pertecente a operadora %s" % dado['operadora']

	print "Número %s - %s, RN %s" % (dado['numero'],tipo,dado['rn1'])

